/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anastasia
 */
public class Article {
    private String goodsName;
    private float goodsPrice;
    
    public Article(String name, float price) {
        this.goodsName = name;
        this.goodsPrice = price;
    }

    Article() {
    }
    
    public void setName(String name) {
        this.goodsName = name;
    }
    
    public String getName() {
        return goodsName;
    }
    
    public void setPrice(float price) {
        this.goodsPrice = price;
    }
    
    public float getPrice() {
        return goodsPrice;
    }
}
