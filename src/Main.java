
import java.util.TreeMap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anastasia
 */
public class Main {
    public static void main(String[] args) {
        TreeMap<Integer, Article> goods = new TreeMap<>();
        View view = new View();
        Controller controller = new Controller(goods, view);
        controller.creating();
        controller.processUser();
    }
    
    
}
